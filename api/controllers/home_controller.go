package controllers

import (
	"net/http"

	"bitbucket.org/hapidznur/golang/api/responses"
)

func (server *Server) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Welcome To Our API")

}
